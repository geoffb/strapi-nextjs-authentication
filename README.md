# Start project
`tilt up`

# Stop project
`tilt down`

# Notes

Following this course: https://www.youtube.com/playlist?list=PL7Q0DQYATmvjXSuHfB8CY_n_oUeqZzauZ

## Installing yarn:
- In empty directory: `yarn init -2`
- Use latest version: `yarn set version stable`

## Installing strapi:
npx create-strapi-app@latest <nameOfProject>

## JWT auth token in strapi

Create a user

To get a json web token (JWT):
- POST : localhost:1337/api/auth/local

Payload: {
    "identifier": "email or username",
    "password": "user password"
}

Then can query Authenticated types:

Query types & populate reviews
- GET:
  - localhost:1337/api/films?populate=reviews
  - localhost:1337/api/films?populate=*  // this is to populate all relations on films type
  
## Installing Nextjs:
npx create-next-app@latest --typescript

## Setup tailwindcss
yarn add -D tailwindcss postcss autoprefixer

Create a conf file with prefixer:<br>
npx tailwindcss init -p

In the tailwind.config.js:
    - `content: ['./src/**/*.tsx']`,

In the global.css:
```
@tailwind base;
@tailwind components;
@tailwind utilities;
```

## Pagination

- GET:
    - localhost:1337/api/films?pagination[page]=1&pagination[pageSize]=3








