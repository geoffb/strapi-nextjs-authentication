export default ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '34f77eab19899a2bec24f69e051a3107'),
  },
});
