import React, { useState } from 'react'
import useSWR from 'swr'
import { fetcher } from 'services/Fetcher'
import { apiUrl } from 'services/env'
import {
  FilmEntity,
  FilmEntityResponseCollection,
} from 'generated/graphql-types'
import Layout from 'components/Layout'
import FilmsList from 'components/FilmsList'

interface IFilms {
  films: Array<FilmEntity>
}

const Films: React.FC<IFilms> = ({ films }) => {
  const [pageIndex, setPageIndex] = useState(1)
  const url = `${apiUrl}/films?pagination[page]=${pageIndex}&pagination[pageSize]=5`
  const { data } = useSWR<FilmEntityResponseCollection>(url, fetcher)

  if (!data) {
    return <Layout>Loading...</Layout>
  }

  return (
    <Layout>
      <h1 className='font-bold text-5xl mb-7'>Films list</h1>
      <span className='block mb-7'>Page number: {pageIndex}</span>
      <FilmsList films={data.data || films} />
      <div className='flex justify-between mt-5'>
        <button
          disabled={pageIndex === 1}
          onClick={() => setPageIndex((prev) => (prev === 1 ? 1 : prev - 1))}
        >
          Previous
        </button>
        <button
          disabled={!data.data.length}
          onClick={() => setPageIndex((prev) => prev + 1)}
        >
          Next
        </button>
      </div>
    </Layout>
  )
}

export default Films

export async function getStaticProps() {
  const filmsResponse = await fetcher<FilmEntityResponseCollection>(
    `${apiUrl}/films?pagination[page]=1&pagination[pageSize]=5`
  )

  return {
    props: {
      films: filmsResponse.data,
    },
  }
}
