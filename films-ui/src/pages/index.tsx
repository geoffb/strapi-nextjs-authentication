import type { NextPage } from 'next'
import Layout from 'components/Layout'

const Home: NextPage = () => (
  <Layout>
    <h1 className='font-bold text-5xl'>Homepage</h1>
    <p>some text</p>
  </Layout>
)

export default Home
