import React from 'react'
import { fetcher } from '../../services/Fetcher'
import { FilmEntity, FilmEntityResponse } from 'generated/graphql-types'
import { apiUrl } from 'services/env'
import Layout from 'components/Layout'

interface IFilm {
  film: FilmEntity
}

const Film: React.FC<IFilm> = ({ film }) => {
  return (
    <Layout>
      <h1 className='font-bold text-5xl mb-7'>{film.attributes?.title}</h1>
    </Layout>
  )
}

export default Film

export async function getServerSideProps({
  params,
}: {
  params: {
    id: number
  }
}) {
  const { id } = params
  const filmResponse = await fetcher<FilmEntityResponse>(
    `${apiUrl}/films/${id}`
  )

  console.log('res', filmResponse.data)

  return {
    props: {
      film: filmResponse.data,
    },
  }
}
