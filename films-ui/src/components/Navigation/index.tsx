import React from 'react'
import Link from 'next/link'
import Image from 'next/image'
import logo from '../../../public/app-logo.png'

interface INavigation {}

const Navigation: React.FC<INavigation> = (props) => (
  <nav className='flex flex-wrap items-center justify-between w-full py-4 md:py-0 px-4 text-lg text-gray-700 bg-white'>
    <div>
      <Link href='/' passHref className='cursor-pointer'>
        <Image alt='logo' src={logo} />
      </Link>
    </div>
    <div className='hidden w-full md:flex md:items-center md:w-auto' id='menu'>
      <ul className='pt-4 text-base text-gray-700 md:flex md:justify-between md:pt-0 space-x-2'>
        <li>
          <Link href='/'>
            <span className='md:pt-2 py-2 block hover:text-purple-400 cursor-pointer'>
              Home
            </span>
          </Link>
        </li>
        <li>
          <Link href='/films'>
            <span className='md:pt-2 py-2 block hover:text-purple-400 cursor-pointer'>
              Films
            </span>
          </Link>
        </li>
      </ul>
    </div>
  </nav>
)

export default Navigation
