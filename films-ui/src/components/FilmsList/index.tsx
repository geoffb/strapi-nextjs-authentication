import React from 'react'
import Link from 'next/link'
import { FilmEntity } from 'generated/graphql-types'

interface IFilmsList {
  films: Array<FilmEntity>
}

const FilmsList: React.FC<IFilmsList> = ({ films }) => (
  <ul className='list-none space-y-4 text-4xl font-bold mb-3'>
    {films.map((film) => (
      <li key={film.id}>
        <Link href={`film/${film.id}`}>{film.attributes?.title}</Link>
      </li>
    ))}
  </ul>
)

export default FilmsList
