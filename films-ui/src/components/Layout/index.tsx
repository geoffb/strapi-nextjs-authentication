import React from 'react'
import Head from 'next/head'
import Navigation from '../Navigation'

interface ILayout {
  children: React.ReactNode
}

const Layout: React.FC<ILayout> = ({ children }) => (
  <>
    <Head>
      <title>Film Database</title>
    </Head>
    <Navigation />
    <main className='px-4'>
      <div className='flex justify-center items-center bg-amber-50 mx-auto w-2/4 rounded-lg my-16 p-16'>
        <div className='text-2xl font-medium'>{children}</div>
      </div>
    </main>
  </>
)

export default Layout
